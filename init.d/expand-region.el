;; =============================================================================
;; Init expand-region 
;; =============================================================================
;; Swap for package install

;; Enable expand region
(require 'expand-region)

;; ============================= Key bindings ==================================
;; Expand region
(global-set-key (kbd "C-=") 'er/expand-region)
