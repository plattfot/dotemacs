(defun setup-home ()
"Splits the session into three frames"
(interactive)
(delete-other-frames)
(delete-other-windows)
(make-frame-command)
(make-frame-command)
)
