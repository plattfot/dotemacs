;; =============================================================================
;; Java
;; =============================================================================

;; ============================= Key bindings ==================================

;; Key bindings for Java autocomplete
(local-set-key (kbd "C-c i") (quote ajc-import-all-unimported-class))
(local-set-key (kbd "C-c m") (quote ajc-import-class-under-point))
