;; =============================================================================
;; TRAMP
;; =============================================================================

;; Enable tramp and setup
(require 'tramp)
(setq tramp-default-method "ssh")

