(require 'sudo-edit)

;;------------------------------------------------------------------------------

(global-set-key (kbd "C-c C-r") 'sudo-edit-current-file)
