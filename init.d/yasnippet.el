;; =============================================================================
;; Init yasnippet
;; =============================================================================

;; Enable yasnippet and setup
(require 'yasnippet) ;; not yasnippet-bundle
(yas-global-mode 1)

;; ;;(yas/load-directory "~/.emacs.d/plugins/yasnippet/snippets")
;; ;; enable java autocomplete and setup
;; ;; (require 'ajc-java-complete-config)
;; ;; (add-hook 'java-mode-hook 'ajc-java-complete-mode)
;; ;; (add-hook 'find-file-hook 'ajc-4-jsp-find-file-hook)

;; ============================= Key bindings ==================================
;; (define-key yas-minor-mode-map (kbd "<tab>") nil)
;; (define-key yas-minor-mode-map (kbd "TAB") nil)
;; (define-key yas-minor-mode-map (kbd "C-o") 'yas-expand)

